PACKAGE=$(shell echo *.spec | sed -e 's/.spec$$//')

.PHONY: sources srpm

sources: $(PACKAGE).spec
	spectool -g $(PACKAGE).spec

srpm: sources $(PACKAGE).spec
	rpmbuild -D '_srcrpmdir ./' -D '_sourcedir ./' -bs $(PACKAGE).spec
